# QMK TOOLS

## Setup
```
qmk setup davidbeijinho/qmk_firmware

qmk compile -kb jj_ergo -km default

qmk config user.keyboard=jj_ergo

qmk config user.keymap=default
```

### Upload
```
bootloadHID -r ~/qmk_firmware/.build/jj_ergo_default.hex


qmk compile -kb jj_ergo -km beiji


qmk compile -kb jj_ergo -km beijo

bootloadHID -r ~/qmk_firmware/.build/jj_ergo_beiji.hex
```