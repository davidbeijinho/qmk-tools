const fs = require('fs');
const { spawnSync } = require('child_process');
const ora = require('ora');

const LAYOUT_CONFIG_FILE = "beijo.json"
const KEYBOARD_CONFIG_FILE = "jj_ergo.json"
const KEYMPAD_DESTINATION = "/home/nomada/qmk_firmware/keyboards/jj_ergo/keymaps/beijo/keymap.c"

const keymapTemplate = (layouts) => `
#include QMK_KEYBOARD_H

enum layer_names {
    BASE, // default layer
    SYMB,  // function layer
    MDIA   // media keys
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [BASE] = LAYOUT(
    ${layouts[0]}
  ),
  [SYMB] = LAYOUT(
    ${layouts[1]}
  ),
  [MDIA] = LAYOUT(
    ${layouts[2]}
  ),
};
`;

const compile = () => {
  const ls = spawnSync('qmk', ['compile', '-kb', 'jj_ergo', '-km', 'beijo']);
  console.log(`stderr: ${ls.stderr.toString()}`);
  console.log(`stdout: ${ls.stdout.toString()}`);
}

const upload = () => {
  const ls = spawnSync('bootloadHID', ['-r', '/home/nomada/qmk_firmware/.build/jj_ergo_beijo.hex']);
  console.log(`stderr: ${ls.stderr.toString()}`);
  console.log(`stdout: ${ls.stdout.toString()}`);
}

const parseKeymap = () => {
  const layoutData = JSON.parse(fs.readFileSync(LAYOUT_CONFIG_FILE, 'utf8'));
  const keyboardData = JSON.parse(fs.readFileSync(KEYBOARD_CONFIG_FILE, 'utf8'));
  return layoutData.layers.map((layer) => layer.map((_, index, keys) => keys[keyboardData.order[index]]).join(","))
}

const writeKeymap = (layouts) => {
  fs.writeFileSync(KEYMPAD_DESTINATION, keymapTemplate(layouts))
}

const waitAndDo = (func) => {
  const spinner = ora('waiting to be in firmware mode').start();
  setTimeout(() => {
    spinner.stop()
    func()
    }, 5000);
}

try {
  const parsedLayouts = parseKeymap()
  writeKeymap(parsedLayouts)
  compile()
  waitAndDo(upload)
} catch (err) {
  console.log(`Error: ${err} `);
}
